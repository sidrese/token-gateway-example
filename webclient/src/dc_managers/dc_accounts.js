const {CryptoUtils, LocalAddress} = require('loom-js/dist');

let accounts = [];

for (let i = 0; i < 10; i++) {
	let prK = CryptoUtils.generatePrivateKey();
	let address = LocalAddress.fromPublicKey(CryptoUtils.publicKeyFromPrivateKey(prK)).toString();
	let privateKey = CryptoUtils.Uint8ArrayToB64(prK);
	accounts.push({
		address,
		privateKey
	})
}

export default {
	getAccounts() {
		return accounts;
	}
}