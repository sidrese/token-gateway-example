import React from 'react'

const {
    CryptoUtils
} = require('loom-js/dist');

export default class ETHTokensCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            transferAmount: 0,
            addressToMap: '0x'
        }
    }

    render() {
        return (
            <div className="card" style={{width: 286, height: 300, float: 'left', margin: 4}}>
                <div className="card-body">
                    <p>Token Balance</p>
                    <p>{this.props.balance}</p>
                    {this.props.mapping ?
                        <div>
                            <p>Mapped Address</p>
                            <p>{CryptoUtils.bytesToHex(this.props.mapping.to.local.bytes)}</p>
                            <input className="col-6 card-text"
                                   type={"number"}
                                   onChange={(event) => {
                                       this.setState({transferAmount: event.target.value})
                                   }}
                                   value={this.state.transferAmount}/>
                            <button
                                type="button"
                                className="col-6 btn btn-primary"
                                onClick={() => this.props.sendToDAppChain(this.state.transferAmount)}>
                                Transfer
                            </button>
                        </div> : ''}
                    <p>Address Mapping</p>
                    <input className="col-12 card-text"
                           type={"text"}
                           onChange={(event) => {
                               this.setState({addressToMap: event.target.value})
                           }}
                           value={this.state.addressToMap}/>
                    <button
                        type="button"
                        className="col-12 btn btn-primary"
                        onClick={() => this.props.sign(this.state.addressToMap)}>
                        Map Address
                    </button>
                </div>
            </div>
        )
    }
}