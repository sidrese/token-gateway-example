import React from 'react'

export default class Wallet extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			balance: 0,
			withdraw: 0,
			transfer: 0,
			transferAddr: ''
		}
	}
	
	componentWillMount() {
		this.props.dcTokenManager.getBalanceOfUserAsync(this.props.address).then(balance => this.setState({balance}));
	}
	
	render() {
		return (
			<div className={"card " + (this.props.mapped ? "bg-success" : "")}
			     style={{width: 286, height: 'auto', float: 'left', margin: 4}}>
				<div className="card-body">
					<p>{this.props.address}</p>
					<p>{this.props.privateKey}</p>
					<p>Balance: {this.state.balance}</p>
					{
						this.state.balance > 0 && this.props.mapped ?
							<div className="section row mb-4">
								<input className="card-text col-4"
								       type={"number"}
								       value={this.state.withdraw}
								       onChange={(e) => {
									       this.setState({withdraw: e.target.value})
								       }}/>
								<button
									type="button"
									className="btn btn-primary col-8"
									onClick={() => this.props.allowToWithdrawFrom(this.props.address, this.props.privateKey, this.state.withdraw)}>
									Allow Withdraw
								</button>
							</div>
							: ""
					}
					{
						this.state.balance > 0 ?
							<div className="section">
								<div className="row">
									<input className="card-text col-12"
									       type={"text"}
									       value={this.state.transferAddr}
									       placeholder="Transfer address"
									       onChange={(e) => {
										       this.setState({transferAddr: e.target.value})
									       }}/>
								</div>
								<div className="row">
									<input className="card-text col-4"
									       type={"number"}
									       value={this.state.transfer}
									       onChange={(e) => {
										       this.setState({transfer: e.target.value})
									       }}/>
									<button
										type="button"
										className="btn btn-primary col-8"
										onClick={() => this.props.transfer(this.state.transferAddr, this.props.address, this.props.privateKey, this.state.transfer)}>
										Transfer
									</button>
								</div>
							</div>
							: ""
					}
				</div>
			</div>
		)
	}
}
