import React from 'react'
import {CryptoUtils} from 'loom-js'
import Wallet from './wallet'
import DAppChainGatewayManager from "../dc_managers/dc_gateway_manager";
import DAppChainAccountManager from "../dc_managers/dc_account_manager";

export default class GatewayTokens extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ethAccount: '0x',
            account: '0x',
            mapping: null,
            balance: 0,
            withdrawing: false,
            privateKey: '0x'
        }
    }

    componentWillMount() {

    }

    async updateUI() {
        const ethAccount = await this.props.ethAccountManager.getCurrentAccountAsync()
        const dcAccountManager = await DAppChainAccountManager.createAsync(this.state.privateKey);
        const dcGatewayManager = await DAppChainGatewayManager.createAsync(this.state.privateKey);

        const mapping = await dcAccountManager.getAddressMappingAsync(ethAccount)
        const account = dcAccountManager.getCurrentAccount()
        const data = await dcGatewayManager.withdrawalReceiptAsync(account)

        let balance
        if (data) {
            balance = +data.value.toString(10)
        }

        this.setState({account, mapping, balance})
    }

    async withdrawFromGateway(amount) {
        this.setState({withdrawing: true})

        const dcGatewayManager = await DAppChainGatewayManager.createAsync(this.state.privateKey)
        const data = await dcGatewayManager.withdrawalReceiptAsync(this.state.account)
        const tokenOwner = data.tokenOwner.local.toString()
        const signature = CryptoUtils.bytesToHexAddr(data.oracleSignature)

        console.log('****************************************');
        console.log(dcGatewayManager);
        console.log(data);
        console.log(tokenOwner);
        console.log(signature);
        console.log('****************************************');

        try {
            await this.props.ethGatewayManager.withdrawTokenAsync(
                tokenOwner,
                amount,
                signature,
                this.props.ethTokenManager.getContractAddress()
            )

            alert('Token withdraw with success, check Owned Tokens')
        } catch (err) {
            console.log('****************************************');
            console.error(err)
            console.log('****************************************');
        }

        this.setState({withdrawing: true})
        await this.updateUI()
    }

    render() {
        const wallet = (
            <Wallet
                balance={this.state.balance}
                action="Withdraw from gateway"
                handleOnClick={() => this.withdrawFromGateway(this.state.balance)}
                disabled={this.state.sending}
            />
        )

        const view = !this.state.mapping ? (
            <p>Please sign your user first</p>
        ) : this.state.balance > 0 ? (
            wallet
        ) : (
            <p>No balance deposited on Gateway yet</p>
        )

        return (
            <div>
                <h2>Ethereum Network Gateway Tokens</h2>
                <div className="container">
                    <input
                        type={"text"}
                        value={this.state.privateKey}
                        onChange={(e) => {
                            this.setState({privateKey: e.target.value})
                        }}
                    />
                    <button
                        type="button"
                        className="col-12 btn btn-primary"
                        onClick={() => this.updateUI()}>
                        Map Address
                    </button>
                    <div>{view}</div>
                </div>
            </div>
        )
    }
}
