import React from 'react'
import Wallet from './wallet'
import DCWallet from './dc_wallet'
import DCAccounts from '../dc_managers/dc_accounts';
import DAppChainAccountManager from "../dc_managers/dc_account_manager";
import DAppChainTokenManager from "../dc_managers/dc_token_manager";
import DAppChainGatewayManager from "../dc_managers/dc_gateway_manager";

const {CryptoUtils} = require('loom-js/dist');
export default class DAppChainTokens extends React.Component {
	
	constructor(props) {
		super(props)
		
		this.state = {
			account: '0x',
			ethAccount: '0x',
			allowing: false
		}
	}
	
	async componentWillMount() {
		this.props.dcGatewayManager.onTokenWithdrawal(async event => {
			alert(`Token ${event.value.toNumber()} ready for withdraw, check Ethereum Gateway`)
			await this.updateUI()
		})
		
		await this.updateUI()
	}
	
	async updateUI() {
		const ethAccount = await this.props.ethAccountManager.getCurrentAccountAsync()
		const account = this.props.dcAccountManager.getCurrentAccount()
		const accounts = DCAccounts.getAccounts();
		const balance = await this.props.dcTokenManager.getBalanceOfUserAsync(account)
		const mapping = await this.props.dcAccountManager.getAddressMappingAsync(ethAccount)
		
		this.setState({account, ethAccount, mapping, balance, accounts})
	}
	
	async allowToWithdraw(amount) {
		this.setState({allowing: true})
		await this.props.dcTokenManager.approveAsync(this.state.account, amount)
		
		try {
			await this.props.dcGatewayManager.withdrawTokenAsync(
				amount,
				this.props.dcTokenManager.getContractAddress()
			)
			
			alert('Processing allowance')
		} catch (err) {
			if (err.message.indexOf('pending') > -1) {
				alert('Pending withdraw exists, check Ethereum Gateway')
			} else {
				console.error(err)
			}
		}
		
		this.setState({allowing: false})
		
		await this.updateUI()
	}
	
	async allowToWithdrawFrom(address, privateKey, amount) {
		
		this.setState({allowing: true})
		const dcTokenManager = await DAppChainTokenManager.createAsync(privateKey);
		await dcTokenManager.approveAsync(address, amount);
		try {
			const dcGatewayManager = await DAppChainGatewayManager.createAsync(privateKey);
			await dcGatewayManager.withdrawTokenAsync(
				amount,
				dcTokenManager.getContractAddress()
			)
			alert('Processing allowance')
		} catch (err) {
			if (err.message.indexOf('pending') > -1) {
				alert('Pending withdraw exists, check Ethereum Gateway')
			} else {
				console.error(err)
			}
		}
		
		this.setState({allowing: false})
		
		await this.updateUI()
	}
	
	async transfer(to, from, privateKey, amount) {
		
		const dcTokenManager = await DAppChainTokenManager.createAsync(privateKey);
		await dcTokenManager.transfer(to, from, amount);
		setTimeout(async () =>{
			await this.updateUI()
		}, 2000);
	}
	
	renderAccounts() {
		if (this.state.accounts)
			return this.state.accounts.map((account) => {
				return (
					<DCWallet
						key={account.address}
						dcTokenManager={this.props.dcTokenManager}
						address={account.address}
						privateKey={account.privateKey}
						allowToWithdrawFrom={(address, privateKey, amount) => this.allowToWithdrawFrom(address, privateKey, amount)}
						transfer={(to, from, privateKey, amount) => this.transfer(to, from, privateKey, amount)}
						mapped={this.state.mapping ? this.state.mapping.to.local.toString() === account.address : false}
					/>
				)
			})
	}
	
	render() {
		const wallet = (
			<Wallet
				balance={this.state.balance}
				action="Allow Withdraw"
				updateBalance={(value) => this.setState({balance: value})}
				handleOnClick={() => this.allowToWithdraw(this.state.balance)}
				disabled={this.state.sending}
			/>
		)
		
		const view = !this.state.mapping ? (
			<p>Please sign your user first</p>
		) : this.state.balance > 0 ? (
			wallet
		) : (
			<p>No balance deposited on DAppChain yet</p>
		)
		
		return (
			<div>
				<div>
					<h2>DAppChain Available Token</h2>
					<div className="container">
						<div>{view}</div>
					</div>
				</div>
				<div>
					<h2>DAppChain Available accounts</h2>
					<div className="container">
						{this.renderAccounts()}
					</div>
				</div>
			</div>
		)
	}
	
}
