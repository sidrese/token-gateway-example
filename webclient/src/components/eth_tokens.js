import React from 'react'
import ETHTokensCard from "./eth_tokens_card";
import DAppChainAccountManager from "../dc_managers/dc_account_manager";

const {
    CryptoUtils
} = require('loom-js/dist');

export default class EthTokens extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            account: '0x',
            mapping: null,
            sending: false,
            balance: 0
        }
    }

    async componentWillMount() {
        await this.updateUI()
    }

    async updateUI() {
        const account = await this.props.ethAccountManager.getCurrentAccountAsync()
        const balance = await this.props.ethTokenManager.getBalanceOfUserAsync(account)
        const mapping = await this.props.dcAccountManager.getAddressMappingAsync(account)

        this.setState({account, balance, mapping})
    }

    async updateMapping() {
        const mapping = await this.props.dcAccountManager.getAddressMappingAsync(this.state.account)
        if (mapping) {
            console.log(mapping);
            console.log('Mapped accounts', mapping.from.toString(), mapping.to.toString())
        }
        this.setState({mapping})
    }

    async sign(foreignAddress) {
        const dcAccountManager = await DAppChainAccountManager.createAsync(foreignAddress)
        await dcAccountManager.signAsync(this.state.account)
        await this.updateMapping()
        //location.reload()
    }

    async sendToDAppChain(amount) {
        this.setState({sending: true})

        try {
            await this.props.ethTokenManager.depositTokenOnGateway(this.state.account, amount)
            alert('The amount will be available on DappChain, check DAppChain ')
        } catch (err) {
            console.log('Transaction failed or denied by user')
        }

        this.setState({sending: false})
        await this.updateUI()
    }

    render() {
        return (
            <ETHTokensCard
                mapping={this.state.mapping}
                balance={this.state.balance}
                sendToDAppChain={(amount) => this.sendToDAppChain(amount)}
                sign={(foreignAddress) => this.sign(foreignAddress)}
            />
        );
    }
}
